import { mount } from "@vue/test-utils";
import Vuetify from "vuetify";

import TradePage from "@/components/Trade/TradePage";

import SpAUDinputPanel from "@/components/SpAUDinputPanel";
import TokeninputPanel from "@/components/TokeninputPanel";

const vuetify = new Vuetify();

describe("Trade Page", () => {
  let wrapper;
  beforeEach(() => {
    wrapper = mount(TradePage, { vuetify });
  });

  it("include Token input field", () => {
    const tokenInputField = wrapper.findComponent(TokeninputPanel);
    expect(tokenInputField.exists()).toBe(true);
  });

  it("include Stablecoin input field", () => {
    const stablecoinInputField = wrapper.findComponent(SpAUDinputPanel);
    expect(stablecoinInputField.exists()).toBe(true);
  });

  it("trigger calculation when token input is filled", () => {
    const spy = jest.spyOn(TradePage.methods, "tokenInput");
    wrapper = mount(TradePage, { vuetify });

    const tokenInput = wrapper.findComponent(TokeninputPanel);

    tokenInput.vm.$emit("EventTokenInputChanged");
    expect(spy).toHaveBeenCalled();
  });

  it("trigger calculation when stablecoin input is filled", () => {
    const spy = jest.spyOn(TradePage.methods, "spAUDInput");
    wrapper = mount(TradePage, { vuetify });

    const stablecoinInput = wrapper.findComponent(SpAUDinputPanel);

    stablecoinInput.vm.$emit("EventspAUDInputChanged");
    expect(spy).toHaveBeenCalled();
  });

  it("trigger switching input field method when btn is clicked", () => {
    const spy = jest.spyOn(TradePage.methods, "swapInputContent");
    wrapper = mount(TradePage, { vuetify });

    const switchBtn = wrapper.find(".switch-btn");

    switchBtn.trigger("click");
    expect(spy).toHaveBeenCalled();
  });
});
