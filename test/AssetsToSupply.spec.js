import { shallowMount } from "@vue/test-utils";
import Vuetify from "vuetify";

import AssetsToSupply from "@/components/Loan/AssetsToSupply";

const vuetify = new Vuetify();

describe("Loan Page Assets to supply", () => {
  let wrapper;
  beforeEach(() => {
    wrapper = shallowMount(AssetsToSupply, {
      mocks: {
        $vuetify: { breakpoint: {} }
      },
      vuetify
    });
  });

  it("dialog is off ", () => {
    expect(wrapper.vm.dialog).toBe(false);
  });

  it("trigger pop-up dialog to appear when supply is selected", async () => {
    const supplyTable = wrapper.find('[data-test="supplyList"]');

    await supplyTable.vm.$emit("click:row", {
      tokenSrc: "Dai.svg",
      tokenName: "Dai",
      tokenCurrency: "DAI",
      apy: 1.21,
      wallet: 23,
      deposit: 0
    });
    expect(wrapper.vm.dialog).toBe(true);
  });

  it("filtering supply with positive balance is off", () => {
    expect(wrapper.vm.hide0Balance).toBe(false);
  });

  it("trigger filtering supply with positive balance ", async () => {
    const filterSwitch = wrapper.find('[data-test="filterSwitch"]');
    await filterSwitch.vm.$emit("change", true);

    expect(wrapper.vm.hide0Balance).toBe(true);
  });
});
