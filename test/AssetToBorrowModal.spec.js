import { mount } from "@vue/test-utils";
import Vuetify from "vuetify";

import AssetToBorrowModal from "@/components/Loan/AssetToBorrowModal";

const vuetify = new Vuetify();

describe("Loan Page Assets to borrow modal", () => {
  let wrapper;
  beforeEach(() => {
    wrapper = mount(AssetToBorrowModal, {
      propsData: {
        selectedBorrow: {
          tokenSrc: "spAUDicon.png",
          tokenName: "spAUD",
          tokenCurrency: "spAUD",
          rate: 0,
          limit: 130,
          apy: 1.2,
          loan: 0,
          limitUsed: 0,
          wallet: 1
        },
        dialog: true
      },
      mocks: {
        $store: { state: { user_connected: true } },
        $vuetify: { breakpoint: {} }
      },
      vuetify
    });
  });

  it("is numeric only", async () => {
    const input = wrapper.find('[data-test="borrowInput"]');
    await input.setValue("hello");
    expect(input.element.value).toBe("");
    await input.setValue("hello12");
    expect(input.element.value).toBe("");
    await input.setValue(12);
    expect(input.element.value).toBe("12");
  });

  it("trigger slider to move for input changes", () => {
    const spy = jest.spyOn(AssetToBorrowModal.methods, "limitInputChanged");
    wrapper = mount(AssetToBorrowModal, {
      propsData: {
        selectedBorrow: {
          tokenSrc: "spAUDicon.png",
          tokenName: "spAUD",
          tokenCurrency: "spAUD",
          rate: 0,
          limit: 130,
          apy: 1.2,
          loan: 0,
          limitUsed: 0,
          wallet: 1
        },
        dialog: true
      },
      mocks: {
        $store: { state: { user_connected: true } },
        $vuetify: { breakpoint: {} }
      },
      vuetify
    });
    const input = wrapper.find('[data-test="borrowInput"]');
    input.setValue(12);
    input.trigger("change");
    expect(spy).toHaveBeenCalled();
  });

  it("trigger modal to close", async () => {
    const closeBtn = wrapper.find('[data-test="closeModal"]');
    await closeBtn.vm.$emit("click");
    expect(wrapper.vm.dialog).toBe(true);
  });

  it("trigger max btn", async () => {
    const spy = jest.spyOn(AssetToBorrowModal.methods, "clickLimit");
    wrapper = mount(AssetToBorrowModal, {
      propsData: {
        selectedBorrow: {
          tokenSrc: "spAUDicon.png",
          tokenName: "spAUD",
          tokenCurrency: "spAUD",
          rate: 0,
          limit: 130,
          apy: 1.2,
          loan: 0,
          limitUsed: 0,
          wallet: 1
        },
        dialog: true
      },
      mocks: {
        $store: { state: { user_connected: true } },
        $vuetify: { breakpoint: {} }
      },
      vuetify
    });
    const maxBtn = wrapper.find('[data-test="maxBtn"]');
    await maxBtn.trigger("click");
    expect(spy).toHaveBeenCalled();
  });
});
