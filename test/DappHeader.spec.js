import { shallowMount, RouterLinkStub } from "@vue/test-utils";
import Vuetify from "vuetify";

import DappHeader from "@/components/DappHeader.vue";
import ConnectWallet from "@/components/ConnectWallet.vue";

const vuetify = new Vuetify();

describe("DappHeader", () => {
  let wrapper;
  beforeEach(() => {
    wrapper = shallowMount(DappHeader, {
      mocks: {
        $vuetify: { breakpoint: {} }
      },
      stubs: {
        NuxtLink: RouterLinkStub
      },
      vuetify
    });
  });

  it("is a Vue instance", () => {
    expect(wrapper.vm).toBeTruthy();
  });

  it("include trade page directory", () => {
    const tradeLink = wrapper.find('[data-test="TRADE"]');
    expect(tradeLink.text()).toBe("TRADE");
    expect(tradeLink.props().to).toBe("/trade");
  });

  it("include loan page directory", () => {
    const loanLink = wrapper.find('[data-test="LOANS"]');
    expect(loanLink.text()).toBe("LOANS");
    expect(loanLink.props().to).toBe("/loan");
  });

  it("include burn page directory", () => {
    const burnLink = wrapper.find('[data-test="BURN"]');
    expect(burnLink.text()).toBe("BURN");
    expect(burnLink.props().to).toBe("/burn");
  });

  it("include connect wallet btn", () => {
    const connectWallet = wrapper.findComponent(ConnectWallet);
    expect(connectWallet.exists()).toBe(true);
  });
});
