import { mount, RouterLinkStub } from "@vue/test-utils";
import Vuetify from "vuetify";

import BurnDetailsDropdown from "@/components/Burn/BurnDetailsDropdown";
import BurnBody from "@/components/Burn/BurnBody";

const vuetify = new Vuetify();

describe("Burn Body", () => {
  let wrapper;
  beforeEach(() => {
    wrapper = mount(BurnBody, {
      mocks: {
        $store: { state: { user_connected: true } },
        $vuetify: { breakpoint: {} }
      },
      stubs: {
        NuxtLink: RouterLinkStub
      },
      vuetify
    });
  });

  it("renders BURN title", () => {
    const burnTitle = wrapper.find(".v-toolbar__title ");
    expect(burnTitle.text()).toBe("BURN");
  });

  it("displays details dropdown when user makes input", async () => {
    await wrapper.setData({ spVOLValue: "10" });
    const detailsDropdown = wrapper.findComponent(BurnDetailsDropdown);
    expect(detailsDropdown.exists()).toBe(true);
  });

  it("details dropdown is not visible for no user input", async () => {
    await wrapper.setData({ spVOLValue: null });
    const detailsDropdown = wrapper.findComponent(BurnDetailsDropdown);
    expect(detailsDropdown.exists()).toBe(false);
  });
});
