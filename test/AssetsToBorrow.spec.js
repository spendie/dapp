import { shallowMount } from "@vue/test-utils";
import Vuetify from "vuetify";

import AssetsToBorrow from "@/components/Loan/AssetsToBorrow";

const vuetify = new Vuetify();

describe("Loan Page Assets to borrow", () => {
  let wrapper;
  beforeEach(() => {
    wrapper = shallowMount(AssetsToBorrow, {
      mocks: {
        $vuetify: { breakpoint: {} }
      },
      vuetify
    });
  });

  it("dialog is off ", () => {
    expect(wrapper.vm.dialog).toBe(false);
  });

  it("trigger pop-up dialog to appear when borrow is selected", async () => {
    const borrowTable = wrapper.find('[data-test="borrowList"]');

    await borrowTable.vm.$emit("click:row", {
      tokenSrc: "Dai.svg",
      tokenName: "Dai",
      tokenCurrency: "DAI",
      apy: 1.21,
      wallet: 23,
      deposit: 0
    });
    expect(wrapper.vm.dialog).toBe(true);
  });

  it("filtering borrow list with positive balance is off", () => {
    expect(wrapper.vm.hide0Balance).toBe(false);
  });

  it("trigger filtering borrow list with positive balance ", async () => {
    const filterSwitch = wrapper.find('[data-test="filterSwitch"]');
    await filterSwitch.vm.$emit("change", true);

    expect(wrapper.vm.hide0Balance).toBe(true);
  });
});
