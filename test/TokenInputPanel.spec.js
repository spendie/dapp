import { shallowMount, mount } from "@vue/test-utils";
import Vuetify from "vuetify";

import TokeninputPanel from "@/components/TokeninputPanel";

const vuetify = new Vuetify();

describe("Token Input Panel", () => {
  let wrapper;
  beforeEach(() => {
    wrapper = mount(TokeninputPanel, { vuetify });
  });

  it("is numeric only", async () => {
    const input = wrapper.find('[data-test="tokenInput"]');
    await input.setValue("hello");
    expect(input.element.value).toBe("");
    await input.setValue("hello12");
    expect(input.element.value).toBe("");
    await input.setValue(12);
    expect(input.element.value).toBe("12");
  });

  it("is positive numeric only", () => {
    const rules = TokeninputPanel.data().priceRules;
    expect(rules[1]("-1")).toBe("Input must be positive");
    expect(rules[1]("0")).toBe("Input must be positive");
    expect(rules[1]("1")).toBe(true);
  });

  it("does not allow empty input", () => {
    const rules = TokeninputPanel.data().priceRules;
    expect(rules[0]("")).toBe("Input is required");
    expect(rules[0]("1")).toBe(true);
  });

  it("trigger pop-up dialog to appear when token button is clicked", () => {
    wrapper = mount(TokeninputPanel, { vuetify });

    const button = wrapper.find("button");

    button.trigger("click");
    expect(wrapper.vm.dialog).toBe(true);
  });
});

describe("Token pop up dialog", () => {
  let wrapper;
  beforeEach(() => {
    wrapper = shallowMount(TokeninputPanel, {
      vuetify
    });
  });

  it("is rendered", async () => {
    await wrapper.setData({ dialog: true });

    const dialog = wrapper.find('[data-test="tokenModal"]');
    expect(dialog.vm.value).toBe(true);
  });

  it("is select token", async () => {
    await wrapper.setData({ dialog: true });

    const tokenTable = wrapper.find('[data-test="tokensList"]');
    await tokenTable.vm.$emit("click:row", {
      tokenSrc: "Dai.svg",
      tokenName: "Dai",
      tokenCurrency: "DAI",
      apy: 1.21,
      wallet: 23,
      deposit: 0
    });
    expect(wrapper.vm.selectedToken).toBe("DAI");
  });

  it("trigger modal to close", async () => {
    await wrapper.setData({ dialog: true });

    const closeBtn = wrapper.find('[data-test="closeModal"]');
    await closeBtn.vm.$emit("click");
    expect(wrapper.vm.dialog).toBe(false);
  });
});
