import { mount } from "@vue/test-utils";
import Vuetify from "vuetify";

import AssetToSupplyModal from "@/components/Loan/AssetToSupplyModal";

const vuetify = new Vuetify();

describe("Loan Page Assets to supply modal", () => {
  let wrapper;
  beforeEach(() => {
    wrapper = mount(AssetToSupplyModal, {
      propsData: {
        selectedSupply: {
          tokenSrc: "Dai.svg",
          tokenName: "Dai",
          tokenCurrency: "DAI",
          apy: 1.21,
          wallet: 23,
          deposit: 0
        },
        dialog: true
      },
      mocks: {
        $store: { state: { user_connected: true } },
        $vuetify: { breakpoint: {} }
      },
      vuetify
    });
  });

  it("is numeric only", async () => {
    const input = wrapper.find('[data-test="tokenInput"]');
    await input.setValue("hello");
    expect(input.element.value).toBe("");
    await input.setValue("hello12");
    expect(input.element.value).toBe("");
    await input.setValue(12);
    expect(input.element.value).toBe("12");
  });

  it("trigger slider to move for input changes", () => {
    const spy = jest.spyOn(AssetToSupplyModal.methods, "limitInputChanged");
    wrapper = mount(AssetToSupplyModal, {
      propsData: {
        selectedSupply: {
          tokenSrc: "Dai.svg",
          tokenName: "Dai",
          tokenCurrency: "DAI",
          apy: 1.21,
          wallet: 23,
          deposit: 0
        },
        dialog: true
      },
      mocks: {
        $store: { state: { user_connected: true } },
        $vuetify: { breakpoint: {} }
      },
      vuetify
    });
    const input = wrapper.find('[data-test="tokenInput"]');
    input.setValue(12);
    input.trigger("change");
    // token_input.vm.$emit("EventTokenInputChanged");
    expect(spy).toHaveBeenCalled();
    // expect(slider).toBe("dijcs");
  });

  it("trigger modal to close", async () => {
    const closeBtn = wrapper.find('[data-test="closeModal"]');
    await closeBtn.vm.$emit("click");
    expect(wrapper.vm.dialog).toBe(true);
  });

  it("trigger max btn", async () => {
    const spy = jest.spyOn(AssetToSupplyModal.methods, "clickLimit");
    wrapper = mount(AssetToSupplyModal, {
      propsData: {
        selectedSupply: {
          tokenSrc: "Dai.svg",
          tokenName: "Dai",
          tokenCurrency: "DAI",
          apy: 1.21,
          wallet: 23,
          deposit: 0
        },
        dialog: true
      },
      mocks: {
        $store: { state: { user_connected: true } },
        $vuetify: { breakpoint: {} }
      },
      vuetify
    });
    const maxBtn = wrapper.find('[data-test="maxBtn"]');
    await maxBtn.trigger("click");
    expect(spy).toHaveBeenCalled();
  });
});
