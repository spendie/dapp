import { mount } from "@vue/test-utils";
import Vuetify from "vuetify";

import BurnPage from "@/components/Burn/BurnPage";

import BurnspVOLinputPanel from "@/components/Burn/BurnspVOLinputPanel";
import SpAUDinputPanel from "@/components/SpAUDinputPanel";

const vuetify = new Vuetify();

describe("Burn Page", () => {
  let wrapper;
  beforeEach(() => {
    wrapper = mount(BurnPage, {
      vuetify
    });
  });

  it("include Token input field", () => {
    const spVOLinputField = wrapper.findComponent(BurnspVOLinputPanel);
    expect(spVOLinputField.exists()).toBe(true);
    // expect(wrapper.findAllComponents(BurnspVOLinputPanel)).toBe("as");
  });

  it("include Stablecoin input field", () => {
    const stablecoinInputField = wrapper.findComponent(SpAUDinputPanel);
    expect(stablecoinInputField.exists()).toBe(true);
  });

  it("trigger calculation when spVOL input is filled", () => {
    const spy = jest.spyOn(BurnPage.methods, "spVOLInput");
    wrapper = mount(BurnPage, { vuetify });

    const spVOLinput = wrapper.findComponent(BurnspVOLinputPanel);

    spVOLinput.vm.$emit("EventspVOLInputChanged");
    expect(spy).toHaveBeenCalled();
  });

  it("trigger calculation when stablecoin input is filled", () => {
    const spy = jest.spyOn(BurnPage.methods, "spAUDInput");
    wrapper = mount(BurnPage, { vuetify });

    const stablecoinInput = wrapper.findComponent(SpAUDinputPanel);

    stablecoinInput.vm.$emit("EventspAUDInputChanged");
    expect(spy).toHaveBeenCalled();
  });

  it("trigger switching input field method when btn is clicked", () => {
    const spy = jest.spyOn(BurnPage.methods, "swapInputContent");
    wrapper = mount(BurnPage, { vuetify });

    const switchBtn = wrapper.find(".switch-btn");

    switchBtn.trigger("click");
    expect(spy).toHaveBeenCalled();
  });
});
