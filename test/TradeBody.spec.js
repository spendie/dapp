import { mount, RouterLinkStub } from "@vue/test-utils";
import Vuetify from "vuetify";

import ConfirmIssue from "@/components/Trade/ConfirmIssueBtn";
import ConfirmSwap from "@/components/Trade/ConfirmSwapBtn";
import ConfirmRedeem from "@/components/Trade/ConfirmRedeemBtn";

import TradeBody from "@/components/Trade/TradeBody";

import TradeDetailsDropdown from "@/components/Trade/TradeDetailsDropdown";

const vuetify = new Vuetify();

describe("Trade Body", () => {
  let wrapper;
  beforeEach(() => {
    wrapper = mount(TradeBody, {
      propsData: {
        tradeFunctionality: "Issue"
      },
      mocks: {
        $store: { state: { user_connected: true } },
        $vuetify: { breakpoint: {} }
      },
      stubs: {
        NuxtLink: RouterLinkStub
      },
      vuetify
    });
  });

  it("renders ISSUE title", async () => {
    await wrapper.setProps({ tradeFunctionality: "Issue" });
    const tradeTitle = wrapper.find(".v-toolbar__title ");
    expect(tradeTitle.text()).toBe("ISSUE");
  });

  it("renders ISSUE Confirm button", async () => {
    await wrapper.setProps({ tradeFunctionality: "Issue" });
    const issueBtn = wrapper.findComponent(ConfirmIssue);
    expect(issueBtn.exists()).toBe(true);
  });

  it("renders SWAP title", async () => {
    await wrapper.setProps({ tradeFunctionality: "Swap" });
    const tradeTitle = wrapper.find(".v-toolbar__title ");
    expect(tradeTitle.text()).toBe("SWAP");
  });

  it("renders SWAP Confirm button", async () => {
    await wrapper.setProps({ tradeFunctionality: "Swap" });
    const swapBtn = wrapper.findComponent(ConfirmSwap);
    expect(swapBtn.exists()).toBe(true);
  });

  it("renders REDEEM title", async () => {
    await wrapper.setProps({ tradeFunctionality: "Redeem" });
    const tradeTitle = wrapper.find(".v-toolbar__title ");
    expect(tradeTitle.text()).toBe("REDEEM");
  });

  it("renders REDEEM Confirm button", async () => {
    await wrapper.setProps({ tradeFunctionality: "Redeem" });
    const redeemBtn = wrapper.findComponent(ConfirmRedeem);
    expect(redeemBtn.exists()).toBe(true);
  });

  it("displays details dropdown when user makes input", async () => {
    await wrapper.setData({ tokenValue: "10" });
    const detailsDropdown = wrapper.findComponent(TradeDetailsDropdown);
    expect(detailsDropdown.exists()).toBe(true);
  });

  it("details dropdown is not visible for no user input", async () => {
    await wrapper.setData({ tokenValue: "" });
    const detailsDropdown = wrapper.findComponent(TradeDetailsDropdown);
    expect(detailsDropdown.exists()).toBe(false);
  });
});
