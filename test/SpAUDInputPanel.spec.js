import { shallowMount, mount } from "@vue/test-utils";
import Vuetify from "vuetify";

import SpAUDinputPanel from "@/components/SpAUDinputPanel";

const vuetify = new Vuetify();

describe("SpAUD Input Panel", () => {
  let wrapper;
  beforeEach(() => {
    wrapper = mount(SpAUDinputPanel, { vuetify });
  });

  it("is numeric only", async () => {
    const input = wrapper.find('[data-test="stablecoinsInput"]');
    await input.setValue("hello");
    expect(input.element.value).toBe("");
    await input.setValue("hello12");
    expect(input.element.value).toBe("");
    await input.setValue(12);
    expect(input.element.value).toBe("12");
  });

  it("is positive numeric only", () => {
    const rules = SpAUDinputPanel.data().priceRules;
    expect(rules[1]("-1")).toBe("Input must be positive");
    expect(rules[1]("0")).toBe("Input must be positive");
    expect(rules[1]("1")).toBe(true);
  });

  it("does not allow empty input", () => {
    const rules = SpAUDinputPanel.data().priceRules;
    expect(rules[0]("")).toBe("Input is required");
    expect(rules[0]("1")).toBe(true);
  });

  it("trigger pop-up dialog to appear when token button is clicked", () => {
    wrapper = mount(SpAUDinputPanel, { vuetify });
    const button = wrapper.find("button");
    button.trigger("click");
    expect(wrapper.vm.dialog).toBe(true);
  });
});

describe("Token pop up dialog", () => {
  let wrapper;
  beforeEach(() => {
    wrapper = shallowMount(SpAUDinputPanel, {
      vuetify
    });
  });

  it("is rendered", async () => {
    await wrapper.setData({ dialog: true });

    const dialog = wrapper.find('[data-test="stablecoinModal"]');
    expect(dialog.vm.value).toBe(true);
  });

  it("is select token", async () => {
    await wrapper.setData({ dialog: true });

    const tokenTable = wrapper.find('[data-test="stablecoinsList"]');

    await tokenTable.vm.$emit("click:row", {
      tokenSrc: "spAUDicon.png",
      tokenName: "spAUD",
      tokenCurrency: "spAUD",
      rate: 0,
      limit: 130,
      apy: 1.2,
      loan: 0,
      limitUsed: 0,
      wallet: 1
    });
    expect(wrapper.vm.selectedToken).toBe("spAUD");
  });

  it("trigger modal to close", async () => {
    await wrapper.setData({ dialog: true });

    const closeBtn = wrapper.find('[data-test="closeModal"]');
    await closeBtn.vm.$emit("click");
    expect(wrapper.vm.dialog).toBe(false);
  });
});
