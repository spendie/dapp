import { mount } from "@vue/test-utils";
import Vuetify from "vuetify";

import BurnspVOLinputPanel from "@/components/Burn/BurnspVOLinputPanel";

const vuetify = new Vuetify();

describe("SpVOL Input Panel in Burn page", () => {
  let wrapper;
  beforeEach(() => {
    wrapper = mount(BurnspVOLinputPanel, { vuetify });
  });

  it("is numeric only", async () => {
    const input = wrapper.find('[data-test="spVOLInput"]');
    await input.setValue("hello");
    expect(input.element.value).toBe("");
    await input.setValue("hello12");
    expect(input.element.value).toBe("");
    await input.setValue(12);
    expect(input.element.value).toBe("12");
  });

  it("is positive numeric only", () => {
    const rules = BurnspVOLinputPanel.data().priceRules;
    expect(rules[1]("-1")).toBe("Input must be positive");
    expect(rules[1]("0")).toBe("Input must be positive");
    expect(rules[1]("1")).toBe(true);
  });

  it("does not allow empty input", () => {
    const rules = BurnspVOLinputPanel.data().priceRules;
    expect(rules[0]("")).toBe("Input is required");
    expect(rules[0]("1")).toBe(true);
  });
});
